

float fuzzyGrade ( float x, float x0, float x1)
{
  float result = 0;
  
  if ( x < x0)
  {
    result = 0;
  } else if ( x >= x1)
  {
    result = 1;
  } else {
    result = (x/(x1-x0)) - (x0/(x1-x0));
  }
  
  return(result);
}

float fuzzyReverseGrade(float x, float x0, float x1)
{
  float result = 0;
 
  if ( x <= x0)
  {
    result = 1;
  } else if ( x >= x1 )
  {
    result = 0;
  
  } else {
    result = (-x/(x1-x0))+(x1/(x1-x0));
  }
  
  return(result);
}

float fuzzyTriangle( float x, float x0, float x1, float x2)
{
  float result = 0;
  
  if ( x <= x0 )
  {
    result = 0;
  } else if ( x == x1 )
  {
    result = 1;
  } else if ((x>x0) && (x<x1))
    result = (x/(x1-x0))-(x0/(x1-x0));
  {
    result = (-x/(x2-x1)) + (x2/(x2-x1));
  }
  
  return(result);
  
}


float fuzzyTrapezoid( float x, float x0, float x1, float x2, float x3)
{
  float result = 0;
  
  if ( x <= x0)
  {
    result = 0;
  } else if ( ( x>=x1) && (x<=x2) )
  {
    result = 1;
  } else if ((x>x0) && (x<x1))
  {
    result = (x/(x1-x0))-(x0/(x1-x0));
  } else {
    result = (-x/(x3-x2))+(x3/(x3-x2));
  }

  return ( result);  
}


float fuzzyAND( float A, float B)
{
  return min(A,B);
}

float fuzzyOR( float A, float B)
{
  return max(A,B); 
}

float fuzzyNOT(float A)
{
  return ( 1.0 - A);
}

void setup()
{
  int nSize = 8;
  int nRange = 25;
  
  // fuzzification
  float mClose      = fuzzyTriangle(nRange, -30, 0, 30);
  float mMedium     = fuzzyTrapezoid(nRange, 10, 30, 50, 70);
  float mFar        = fuzzyGrade(nRange, 50, 70);
  
  float mTiny       = fuzzyTriangle(nSize, -10, 0, 10);
  float mSmall      = fuzzyTrapezoid(nSize, 2.5, 10, 15, 20);
  float mModerate   = fuzzyTrapezoid(nSize, 15, 20, 25, 30);
  float mLarge      = fuzzyGrade(nSize, 25, 30);
  
  // partial rules (book)
  //float mThreatLow = fuzzyOR(fuzzyAND(mMedium, mTiny), fuzzyAND(mMedium, mSmall));
  //float mThreatMedium = fuzzyAND(mClose, mTiny);
  //float mThreatHigh = fuzzyAND(mClose, mSmall);
  
  // full rules
  float mThreatLow = fuzzyOR(fuzzyAND(mFar,mModerate),fuzzyOR(fuzzyOR(fuzzyAND(mFar,mTiny),fuzzyAND(mFar,mSmall)),fuzzyOR(fuzzyAND(mMedium, mTiny), fuzzyAND(mMedium, mSmall))));
  float mThreatMedium = fuzzyOR(fuzzyAND(mFar,mLarge),fuzzyOR(fuzzyAND(mMedium,mModerate),fuzzyAND(mClose, mTiny)));
  float mThreatHigh = fuzzyOR(fuzzyOR(fuzzyAND(mClose,mSmall),fuzzyAND(mClose,mModerate)),fuzzyOR(fuzzyAND(mClose,mLarge),fuzzyAND(mMedium,mLarge)));
  
  
  // Defuzzification
  float nDeploy = ( mThreatLow * 10 + mThreatMedium * 30 + mThreatHigh*50) / (mThreatLow+mThreatMedium+mThreatHigh);
  
  println( "Close", mClose);
  println( "Medium", mMedium);
  println( "Far", mFar);
  println( "Tiny", mTiny);
  println( "Small", mSmall);
  println( "Moderate", mModerate);
  println( "Large", mLarge);
  
  println();
  
  println( "ThreatLow", mThreatLow);
  println( "ThreatMedium", mThreatMedium);
  println( "ThreatHigh", mThreatHigh);
  
  println();
  
  println("Deploy", nDeploy);
  

}




