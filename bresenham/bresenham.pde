

ArrayList<PVector> bresenham( int col, int row, int endCol, int endRow)
{
  int nextCol=col;
  int nextRow=row;
  int deltaRow=endRow-row;
  int deltaCol=endCol-col;
  int stepCol, stepRow;
  int currentStep, fraction;

  currentStep=0;
  
  if (deltaRow <  0) stepRow=-1;  else stepRow=1;
  if (deltaCol <  0) stepCol=-1;  else stepCol=1;
  deltaRow=abs(deltaRow*2);
  deltaCol=abs(deltaCol*2);

  currentStep++;
    
  ArrayList<PVector> path = new ArrayList<PVector>();
    
  if (deltaCol >deltaRow)
  {
    fraction = deltaRow *2-deltaCol;
    while (nextCol != endCol)
    {
      if (fraction >=0)
      {
        nextRow =nextRow +stepRow;
        fraction =fraction -deltaCol;
      }
      nextCol=nextCol+stepCol;
      fraction=fraction +deltaRow;
      
      path.add(new PVector(nextCol, nextRow));
      
      //println(currentStep, fraction, nextCol, nextRow );      
            
      currentStep++;
    }
  }
  else
  {
    fraction =deltaCol *2-deltaRow;
    while (nextRow !=endRow)
    {
      if (fraction >=0)
      {
       nextCol=nextCol+stepCol;
       fraction=fraction -deltaRow;
      }
     nextRow =nextRow +stepRow;
     fraction=fraction +deltaCol;
     
     path.add(new PVector(nextCol, nextRow));
     
     currentStep++;
    }
  }
  
  return(path);
}

void setup()
{  
  ArrayList<PVector> path = bresenham(2,2,24,4);
  
  for (int i = 0; i < path.size(); i++)
  {
    PVector step = path.get(i);
    println(i,int(step.x),int(step.y));
  }
  
}