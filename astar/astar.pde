//
// Arrow Keys to Move
// Space key to Calculate Bresenham Line
// 'N' key to step thru' A* calculations
//

import java.util.Collections;
import java.util.Comparator;


int WIDTH = 800;
int HEIGHT = 600;
int TILESIZE = 50;

ArrayList<Point> wall = new ArrayList<Point>();
ArrayList<Point> path = new ArrayList<Point>();;
ArrayList<Point> track = new ArrayList<Point>();;

ArrayList<Score> open = new ArrayList<Score>();;
ArrayList<Score> closed = new ArrayList<Score>();;


class Point
{
    int x;
    int y;
    
    Point( int x, int y)
    {
      this.x = x;
      this.y = y;
    }
}

class Score extends Point {

  int c, h;
  Score parent;
  
  Score(int x, int y, int c, int h)
  {
    super(x,y);
    this.c = c;
    this.h = h;
  }
  
  void setParent(int X, int Y)
  {
    //parent = new (X,Y);
  }
}

final Comparator<Score> compByCH = new Comparator<Score>() {
  int compare(Score a, Score b) {
    return (int) ((a.c + a.h) - (b.c + b.h));
  }
};

class Character
{
  int x, y, s;
  color c;
  int X, Y;
  
  Character (int X, int Y)
  {
    this.X = X;
    this.Y = Y;
    
    this.s = floor(TILESIZE / 2);

  }  
  
  void setColor(int r, int g, int b)
  {
    c = color( r,g,b);
  }
  
  void move(int X, int Y)
  {
    this.X = X;
    this.Y = Y;
  }
  
  void up()
  {
    this.Y--;
  }
  
  void down()
  {
    this.Y++;
  }
  
  void left()
  {
    this.X--;
  }
  
  void right()
  {
    this.X++;
  }
  
  void update ()
  {
    int x = (this.X + 1 ) * TILESIZE - this.s;
    int y = (this.Y + 1 ) * TILESIZE - this.s;
   
    fill(c);
    ellipse(x, y, TILESIZE-5, TILESIZE-5);
  }
}

Character monster = new Character(0, 0);
Character hero = new Character(10, 10);

void setup()
{
  size(800, 600);
  frameRate(30);
  rectMode(RADIUS);
  
  prepWall();
  
  monster.setColor( 50, 50, 50);
  hero.setColor( 255, 0, 0);

  //open.add(new Score(2,5));
  
}

void prepWall()
{
  for ( int i = 5; i < 15; i++)
  {
    wall.add(new Point(i,5));
  }
}

void wall()
{
  int x,y;
  
  for (int i = 0; i < wall.size() ; i++)
  {
        Point p = wall.get(i);
      
        x = (p.x + 1) * TILESIZE - floor(TILESIZE / 2);
        y = (p.y + 1) * TILESIZE - floor(TILESIZE / 2);
        
        fill(0);
        rect(x,y, floor(TILESIZE / 2), floor(TILESIZE / 2));
  } 
}

void grid()
{
  background(126);
  
  int s = floor(TILESIZE / 2);
  
  int y = s;
  int x = s;
  
  fill(255); 
  rectMode(RADIUS);
  
  while ( y < HEIGHT )
  {
    fill(255); 
    rect(x , y , s, s );  
  
    fill(0);
    point(x,y);

    if ( x > WIDTH )
    {
      y = y + TILESIZE;
      x = s;
    } else {
      x = x + TILESIZE;
    }  
  }
}

void track()
{
  int x,y;
  
  for (int i = 0; i < track.size() ; i++)
  {
        Point p = track.get(i);
      
        x = (p.x + 1) * TILESIZE - floor(TILESIZE / 2);
        y = (p.y + 1) * TILESIZE - floor(TILESIZE / 2);
        
        fill(0,0,255);
        ellipse(x,y, 10,10);
      }
}  

void path()
{
  int x,y;
  
  for (int i = 0; i < path.size() ; i++)
  {
        Point p = path.get(i);
      
        x = (p.x + 1) * TILESIZE - floor(TILESIZE / 2);
        y = (p.y + 1) * TILESIZE - floor(TILESIZE / 2);
        
        fill(0,255,0);
        rect(x,y, floor(TILESIZE / 2), floor(TILESIZE / 2));
      }
}  

void drawOpen()
{
  int x,y;
  
  for (int i = 0; i < open.size() ; i++)
  {
        Score s = open.get(i);
      
        x = (s.x + 1) * TILESIZE - floor(TILESIZE / 2);
        y = (s.y + 1) * TILESIZE - floor(TILESIZE / 2);
        
        fill(200,100,0);
        rect(x,y, floor(TILESIZE / 2), floor(TILESIZE / 2));

        fill(0);
        text(s.c + s.h, x - floor(TILESIZE / 2) + 3, y - floor(TILESIZE / 2) + 15);
        text(s.c, x - floor(TILESIZE / 2) + 3, y + floor(TILESIZE / 2) - 7);
        text(s.h, x + floor(TILESIZE / 2) - 27, y + floor(TILESIZE / 2) - 7);
  } 
}

void drawClosed()
{
  int x,y;
  
  for (int i = 0; i < closed.size() ; i++)
  {
        Score s = closed.get(i);
      
        x = (s.x + 1) * TILESIZE - floor(TILESIZE / 2);
        y = (s.y + 1) * TILESIZE - floor(TILESIZE / 2);
        
        fill(200,0,100);
        rect(x,y, floor(TILESIZE / 2), floor(TILESIZE / 2));

        fill(0);
        text(s.c + s.h, x - floor(TILESIZE / 2) + 3, y - floor(TILESIZE / 2) + 15);
        text(s.c, x - floor(TILESIZE / 2) + 3, y + floor(TILESIZE / 2) - 7);
        text(s.h, x + floor(TILESIZE / 2) - 17, y + floor(TILESIZE / 2) - 7);
  } 
}

Score minOpen()
{
  //ArrayList<Score> s = new ArrayList<Point>();
  int n = 1;
  Score min = open.get(0);

  while ( n < open.size() )
  {  
    if ( ( min.c + min.h ) >= (open.get(n).c + open.get(n).h) )
    {
      min = open.get(n);
    } 
      n++;
    
  }

  println(min.x, min.y, min.c, min.h);
  return(min);
}

boolean isWall( int x, int y)
{
  boolean found = false;
  
  for (int i =0; i < wall.size(); i++)
  {
    if ( x == wall.get(i).x && y == wall.get(i).y )
    {
        found = true;
        break;
    }
    
  }
  return(found);
}

boolean isClosed( int x, int y)
{
  boolean found = false;
  
  for (int i =0; i < closed.size(); i++)
  {
    if ( x == closed.get(i).x && y == closed.get(i).y )
    {
        found = true;
        break;
    }
    
  }
  return(found);
}

boolean isOpen( int x, int y)
{
  boolean found = false;
  
  for (int i =0; i < open.size(); i++)
  {
    if ( x == open.get(i).x && y == open.get(i).y )
    {
        found = true;
        break;
    }
    
  }
  return(found);
}

boolean hasArrived()
{
  boolean found = false;
  
  for (int i =0; i < closed.size(); i++)
  {
    if ( monster.X == closed.get(i).x && monster.Y == closed.get(i).y )
    {
        found = true;
        break;
    }
    
  }
  return(found);
}
  
void astar()
{  
  int g, c, h;
  
  int X;
  int Y;
  
  Score min;
  
  if ( hasArrived() )
  {
    int i = closed.size() - 1;
    int n = 0;
    
    //Score s = closed.get(i);
    //println( s.x, s.y, (s.c + s.h), s.parent.x, s.parent.y);
   
    println("DONE",i);
    Score s;
    
    while ( i > 0 )
    {
      s = closed.get(i);
      println(s.x,s.y, s.parent.x, s.parent.y);
      i--;
    }
    
    s = closed.get(closed.size() - 1);
    while ( !(s.x == hero.X && s.y == hero.Y) )
    {
      s = s.parent;
      //println("track", s.x, s.y, hero.X, hero.Y, (s.x == hero.X), (s.y == hero.Y));
      track.add(new Point(s.x,s.y));
      
      n++;
    }
    
  } else {
  
  if ( open.size() == 0 )
  {
    X = hero.X;
    Y = hero.Y;
    
    min = new Score(X,Y,0,0);
    
    closed.add(min);
    
  }  else {
    min = minOpen();

    closed.add(min);
    open.remove(min);
    
    X = min.x;
    Y = min.y;
  } 
    
  if ( isWall(X,Y) )
  {
    closed.remove(min);
    
  } else { 
    
    h = (bresenham( X - 1, Y, monster.X, monster.Y).size() - 2) * 10;
    c =  min.c + 10 ;

    //c = (bresenham( X - 1, Y, hero.X, hero.Y).size() - 2) * 10 ;
    // check if already in closed or wall
    if ( !isClosed(X-1,Y) && !isOpen(X - 1, Y))
    {
      Score s = new Score(X - 1, Y, c, h );
      //s.setParent(X,Y);
      s.parent = min;
      open.add(s);
    }
    
    h = (bresenham( X - 1, Y - 1, monster.X, monster.Y).size() - 2) * 10;
    //c = (bresenham( X - 1, Y - 1, hero.X, hero.Y).size() - 2) * 14 ;
    c = min.c + 14;
    if ( !isClosed(X-1,Y - 1)  && !isOpen(X-1, Y-1) )
    {
      Score s = new Score(X - 1, Y - 1, c, h );
//      s.setParent(X,Y);
      s.parent = min;

      open.add(s);
    }
    
    h = (bresenham( X, Y - 1, monster.X, monster.Y).size() - 2) * 10;
    //c = (bresenham( X, Y - 1, hero.X, hero.Y).size() - 2) * 10 ;
    c = min.c + 10;
    if ( !isClosed(X,Y-1) && !isOpen(X, Y-1))
    {
      Score s = new Score(X, Y - 1, c, h );
//      s.setParent(X,Y);
            s.parent = min;

      open.add(s);  
    }
    
    h = (bresenham( X + 1, Y - 1, monster.X, monster.Y).size() - 2) * 10;
    //c = (bresenham( X + 1, Y - 1, hero.X, hero.Y).size() - 2) * 14 ;
    c = min.c + 14;
    if ( !isClosed(X+1,Y-1) && !isOpen(X+1 , Y-1))
    {
      Score s =new Score(X + 1, Y - 1, c, h );
//      s.setParent(X,Y);
        s.parent = min;

      open.add(s); 
    }
    
    h = (bresenham( X+1, Y, monster.X, monster.Y).size() - 2) * 10;
    //c = (bresenham( X+1, Y, hero.X, hero.Y).size() - 2) * 10 ;
    c = min.c + 10;
    if ( !isClosed(X+1,Y) && !isOpen(X+1 , Y))
    {
      Score s = new Score(X+1, Y, c, h ); 
//      s.setParent(X,Y);
        s.parent = min;

      open.add(s); 
    }
    
    h = (bresenham( X+1, Y+1, monster.X, monster.Y).size() - 2) * 10;
    //c = (bresenham( X+1, Y+1, hero.X, hero.Y).size() - 2) * 14 ;
    c = min.c + 14;
    if ( !isClosed(X+1,Y+1) && !isOpen(X+1 , Y+1))
    {
      Score s = new Score(X+1, Y+1, c, h );
//      s.setParent(X,Y);
        s.parent = min;

      open.add(s); 
    }
    
    h = (bresenham( X, Y+1, monster.X, monster.Y).size() - 2) * 10;
    //c = (bresenham( X, Y+1, hero.X, hero.Y).size() - 2) * 10 ;
    c = min.c + 10;
    if (!isClosed(X,Y+1) && !isOpen(X , Y+1))
    {
      Score s = new Score(X, Y+1, c, h );
//      s.setParent(X,Y);
        s.parent = min;

      open.add(s); 
    }
    
    h = (bresenham( X-1, Y+1, monster.X, monster.Y).size() - 2) * 10;
    //c = (bresenham( X-1, Y+1, hero.X, hero.Y).size() - 2) * 14 ;
    c = min.c + 14;
    if (!isClosed(X-1,Y+1) && !isOpen(X-1 , Y+1))
    {
      Score s = new Score(X-1, Y+1, c, h );
//      s.setParent(X,Y);
        s.parent = min;

      open.add(s); 
    }
  } 
  } 
    //Collections.sort(open, compByCH);
    println("open = ", open.size());
    println("closed = ", closed.size());

}
  

void draw()
{
  grid();
  //wall();
  
  path();  
   
  drawOpen();
  drawClosed();
  
  track();
    
  wall();
  
  monster.update();
  hero.update();
}

void keyPressed()
{
  if (key == CODED)
  {
    if (keyCode == UP)
    {
      monster.up();   
    } else if (keyCode == DOWN)
    {
      monster.down();
    
    }  else if (keyCode == LEFT)
    {
      monster.left();

    } else if (keyCode == RIGHT)
    {
      monster.right();
    } 
  } else {
   
    if ( keyCode == 32 )
    {
      ArrayList<Point> line;
      
      path = bresenham( hero.X, hero.Y, monster.X, monster.Y);      
    
    } else if (keyCode == 78) {
  
       astar();
    }    
    
  }
}


ArrayList<Point> bresenham(int ax, int ay, int bx, int by)
{
  // line eq : y = m * x + c

  ArrayList<Point> line = new ArrayList<Point>();

  // slope
  float m = (( by - ay ) * 1.0) / ( bx- ax );
  float y = ay;
  float x = ax;
  float c = by - m * bx;
  
  int steps = abs(bx - ax) + 1 ;
  
  int dx = abs(bx - ax);
  int dy = abs(by - ay);
  
  if ( dx >= dy )
    steps = dx + 1;
  else
    steps = dy + 1;
    
  for (int i = 0; i < steps; i++)
  {
    //println( x, ", ", y, ", ", round(y));
    line.add ( new Point(int(x),round(y)));
   
    if ( dx >= dy )
    {      
        y = m * x + c;  
        
        if ( ax <= bx )
          x++;
        else
          x--;
          
    } else {
      
        x = ( y - c) /m;
        
        if ( ay <= by )
          y++;
        else
          y--;
    }
    
  }
  
  line.add(new Point(bx,by));
   
  /* 
  for (int i = 0; i < line.size() ; i++)
  {
      Point p = line.get(i);
      
      println(p.x, ", ", p.y); 
  }
  */

  return(line);
}


