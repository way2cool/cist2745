

int predatorX;
int predatorY;
int preyX;
int preyY;

void setup()
{
 size(600,600); 
 frameRate(60);
 
 predatorX = 0;
 predatorY = 0;
 
}

void draw()
{
  background(100);
  fill(255);
  preyX = mouseX;
  preyY = mouseY;
  
  ellipse(preyX, preyY, 50, 50);
  
  fill(255,0,0);
  ellipse(predatorX, predatorY, 50, 50); 
  
  if (predatorX > preyX)
     predatorX--;
  else if (predatorX < preyX)
     predatorX++;
  if (predatorY > preyY)
     predatorY--;
  else if (predatorY < preyY)
     predatorY++; 
}