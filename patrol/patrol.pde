

ArrayList<PVector> bresenham( int col, int row, int endCol, int endRow)
{
  int nextCol=col;
  int nextRow=row;
  int deltaRow=endRow-row;
  int deltaCol=endCol-col;
  int stepCol, stepRow;
  int currentStep, fraction;

  currentStep=0;
  
  if (deltaRow <  0) stepRow=-1;  else stepRow=1;
  if (deltaCol <  0) stepCol=-1;  else stepCol=1;
  deltaRow=abs(deltaRow*2);
  deltaCol=abs(deltaCol*2);

  currentStep++;
    
  ArrayList<PVector> path = new ArrayList<PVector>();
    
  if (deltaCol >deltaRow)
  {
    fraction = deltaRow *2-deltaCol;
    while (nextCol != endCol)
    {
      if (fraction >=0)
      {
        nextRow =nextRow +stepRow;
        fraction =fraction -deltaCol;
      }
      nextCol=nextCol+stepCol;
      fraction=fraction +deltaRow;
      
      path.add(new PVector(nextCol, nextRow));
      
      //println(currentStep, fraction, nextCol, nextRow );      
            
      currentStep++;
    }
  }
  else
  {
    fraction =deltaCol *2-deltaRow;
    while (nextRow !=endRow)
    {
      if (fraction >=0)
      {
       nextCol=nextCol+stepCol;
       fraction=fraction -deltaRow;
      }
     nextRow =nextRow +stepRow;
     fraction=fraction +deltaCol;
     
     path.add(new PVector(nextCol, nextRow));
     
     currentStep++;
    }
  }
  
  return(path);
}

int target = 1;
int step = 0;
ArrayList<PVector> waypoints;
PVector pos;
ArrayList<PVector> path;

void setup()
{  
  frameRate(120);
  size(600,600);  
  
  waypoints = new ArrayList<PVector>();
  
  waypoints.add(new PVector(100,300));
  waypoints.add(new PVector(100,500));
  waypoints.add(new PVector(500,500));
  waypoints.add(new PVector(500,300));
  
  pos = new PVector(500,500);
 
  path = bresenham(int(pos.x),int(pos.y),int(waypoints.get(target).x),int(waypoints.get(target).y));
  
}

void draw()
{
  background(100,100,100);
  fill(255,0,0);
  
  pos = path.get(step);
  ellipse(int(pos.x),int(pos.y),50,50);
  
  if ( ( pos.x == waypoints.get(target).x ) && ( pos.y == waypoints.get(target).y) )
  {
    target++;
    if ( target >= 4 )
    {
        target = 0;
    }
    
    path = bresenham(int(pos.x),int(pos.y),int(waypoints.get(target).x),int(waypoints.get(target).y));
    
    step = 0;
    
  } else {
    step++;
  }
  
}