

ArrayList<PVector> bresenham( int col, int row, int endCol, int endRow)
{
  ArrayList<PVector> path = new ArrayList<PVector>();
  
  int deltaRow=endRow-row;
  int deltaCol=endCol-col;
  
  double m = deltaRow / (double) deltaCol;
  double r = row;
  
  for ( int c=col; c<=endCol; c++)
  {
    path.add(new PVector(c,(int)r) );
    r += m;
  }
  
  return(path);
}

void setup()
{
  ArrayList<PVector> path = bresenham(0,1,26,9);
  
  for (int i = 0; i < path.size(); i++)
  {
    PVector step = path.get(i);
    println(i,int(step.x),int(step.y));
  }
}