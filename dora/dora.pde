
 
class Point
{
    int x;
    int y;
    
    Point( int x, int y)
    {
      this.x = x;
      this.y = y;
    }
}
 
ArrayList<Point> line;
int step = 0;
 
Character hero = new Character(320, 320);
Character monster = new Character(20, 20);
 
 
void setup()
{
  size(800, 600);
  frameRate(30);
  
  line = new ArrayList<Point>();
 
  monster.setColor( 0, 100, 10);
  monster.setImage("zombie-hi.png");
  hero.setImage("dora.png");
 
  hero.setColor(50,50,50);
  
  bresenham( 40, 40, 181, 180 );
}
 
void draw()
{
  background(126);
 
  int d = 1; 
  Point p = line.get(step);
  
  monster.move( p.x, p.y);
  
  
  if ( step < (line.size()-1) )
  {
    if ( dist(monster.x, monster.y, hero.x, hero.y) < 100 )
    {
      step += 10;  
    } else {
    
      step = step + 1;
    }
    
    if ( step > (line.size()-1) )
    {
      step = (line.size()-1);
    }
    
  } 
  
  monster.update();
  hero.update();
  
  line(monster.x, monster.y, hero.x, hero.y);
  
}
 
void bresenham(int ax, int ay, int bx, int by)
{
  // line eq : y = m * x + c
  
  // slope
  float m = (( by - ay ) * 1.0) / ( bx- ax );
  float y = ay;
  float x = ax;
  float c = by - m * bx;
  
  int steps = abs(bx - ax) + 1 ;
  
  int dx = abs(bx - ax);
  int dy = abs(by - ay);
  
  if ( dx >= dy )
    steps = dx + 1;
  else
    steps = dy + 1;
    
  for (int i = 0; i < steps; i++)
  {
    //println( x, ", ", y, ", ", round(y));
    line.add ( new Point(int(x),round(y)));
   
    if ( dx >= dy )
    {      
        y = m * x + c;  
        
        if ( ax <= bx )
          x++;
        else
          x--;
          
    } else {
      
        x = ( y - c) /m;
        
        if ( ay <= by )
          y++;
        else
          y--;
    }
    
  }
  
 /* 
  for (int i = 0; i < line.size() ; i++)
  {
      Point p = line.get(i);
      
      println(p.x, ", ", p.y);
    
  }
  */
 
}
 
 
 
class Character
{
  int x, y;
  color c;
  PImage img = null;
  
  Character (int xx, int yy)
  {
    x = xx;
    y = yy;
  }  
  
  void setImage(String name)
  {
     img = loadImage(name);
  }
  
  void setColor(int r, int g, int b)
  {
    c = color( r,g,b);
  }
  
  void move(int x, int y)
  {
    this.x = x;
    this.y = y;
  }
  
  void update ()
  {
    if ( img == null )
    {
      fill(c);
      ellipse(this.x, this.y, 50, 50);
    } else {
        image(img, this.x, this.y, 50, 50);
 
    }
  }
}
 
 
 
void keyPressed()
{
  if (key == CODED)
  {
    if (keyCode == UP)
    {
      hero.y -= 15;  
    } else if (keyCode == DOWN)
    {
      hero.y += 15;
    }  else if (keyCode == LEFT)
    {
      hero.x -= 15;
    } else if (keyCode == RIGHT)
    {
      hero.x += 15;
    } 
    
    line.clear();
    bresenham( monster.x, monster.y, hero.x, hero.y);
    step = 0;
    //println( monster.x, ", ", monster.y, " : " , hero.x, ", ", hero.y);
    
  } 
}